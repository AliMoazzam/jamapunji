<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function cal($id)
    {
        switch($id)
        {
            case 1:
            return view('calculator.cal1');
            break;

            case 2:
            return view('calculator.cal2');
            break;

            case 3:
            return view('calculator.cal3');           
            break;
        }
    }

    public function compoundinterest(Request $request)
    {
        $amount = $request->amount;
        $annual_interest_rate = $request->annual_interest_rate;
        $number_of_years = $request->number_of_years;
        $compounding = $request->compounding;

        $interval = $compounding == 1 ? "Daily" 
                    : $compounding == 2 ? "Monthly" 
                    : $compounding == 3 ? "Quarterly" 
                    : $compounding == 4 ? "Bi-Annually" 
                    : "Annually";

        $compound_amount = $this->compoundint($amount, $annual_interest_rate, $number_of_years, $compounding);

        $result = "Compound amount would be $compound_amount 
                    at interest rate $annual_interest_rate 
                        for $number_of_years compounded $interval";

        session()->flash("data", $result);
        
        return redirect("/calculator/2");
    }

    public function timeval(Request $request)
    {
        
        $option = $request->options;
        $amount = $request->amount;
        $annual_interest_rate = $request->annual_interest_rate;
        $number_of_years = $request->number_of_years;

        $result = 0;

        if($option == 1)
        {
            $result = "";
        } else {

        }

        session()->flash("data", [
            "options"=>$option,
            "amount"=>$amount,
            "annual_interest_rate"=>$annual_interest_rate,
            "number_of_years"=>$number_of_years
        ]);
        
        return redirect("/calculator/3");
    }

    public function investmentreturn(Request $request)
    {
        $amount = $request->amount;
        $annual_interest_rate = $request->annual_interest_rate;
        $number_of_years = $request->number_of_years;
        $compounding = $request->compounding;

        $result = 0;

        session()->flash("data", [
            "amount"=>$amount,
            "annual_interest_rate"=>$annual_interest_rate,
            "number_of_years"=>$number_of_years,
            "compounding"=>$compounding
        ]);
        
        return redirect("/calculator/1");
    }

    private function compoundint($principal, $interest_rate, $periods, $compounding)
    {
        switch($compounding)
        {
            case 1; //daily
            $interest_rate /= 365;
            $periods *= 365;
            break;

            case 2; //monthly
            $interest_rate /= 12;
            $periods *= 12;

            break;

            case 3; //quarterly
            $interest_rate /= 4;
            $periods *= 4;
            break;

            case 4; //semi annual
            $interest_rate /= 2;
            $periods *= 2;
            break;
        }
        $r = $principal * pow(1+$interest_rate, $periods);
        return $r;
    }

    private function timevalueofmoney()
    {

    }

    private function investreturn()
    {

    }
}
