@extends('layout')

@section('content')

<div class="container">
    <h1 class="display-4 mt-4 ml-2">Compound Interest Calculator</h1>
    <p class="ml-2">
        Compound interest calculator allows gaining interest on the money deposited and the interest already earned. 
        Compound Interest calculator will also help the user to find out upon saving a set amount of money, 
        what he’d be able to save in the long run and how ing would increase savings.
    </p>
</div>
    
<div class="container-fluid" style="margin-top:50px;">
    <div class="d-flex justify-content-center">
        
        <div class="container bg-light mb-5">
            <div class="row">
            
            <div class="col-lg">

                <form action="/calculator/compoundinterest" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md p-3">
                            <div class="form-group">
                                <label for="amount">Principal Amount <span class="text-danger">*</span></label>
                                <input type="text" name="amount" id="amount" class="form-control rounded-0" placeholder="Amount" required>
                            </div>
                        
                            <div class="form-group">
                                <label for="annual_interest_rate">Annual Interest Rate(%) <span class="text-danger">*</span></label>
                                <input type="text" name="annual_interest_rate" id="annual_interest_rate" class="form-control rounded-0" placeholder="Interest Rate" required>
                            </div>
                        
                            <div class="form-group">
                                <label for="number_of_years">Number of Years <span class="text-danger">*</span></label>
                                <input type="text" name="number_of_years" id="number_of_years" class="form-control rounded-0" placeholder="Number of Years" required>
                            </div>

                            <div class="form-group">
                                <label for="compounding">Compounding <span class="text-danger">*</span></label>
                                <select name="compounding" id="compounding" class="form-control">
                                    <option value="" selected>Select</option>
                                    <option value="1">Daily</option>
                                    <option value="2">Monthly</option>
                                    <option value="3">Quarterly</option>
                                    <option value="4">Bi-Annually</option>
                                    <option value="5">Annually</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success rounded-0">Submit</button>
                    <button type="reset" class="btn btn-success rounded-0">Reset</button>
                </form>
            </div>

            <div class="col-lg bg-white">
                <div class="bg-light p-2">
                    <h3>Result</h3>
                </div>
                <div class="p-2">
                    @if(Session::has('data'))
                        {{session('data')['amount']}}
                    @endif
                </div>
            </div>
        
            </div>
        </div>

    </div>
</div>

@endsection