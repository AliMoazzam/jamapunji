@extends('layout')

@section('content')

<div class="container">
    <h1 class="display-4 mt-4 ml-2">Time Value Money Calculator</h1>
    <p class="ml-2">
        How readily an investment grows in a given period of time is entirely dependent on the 
        rate of return earned annually. The time value of money 
        calculator helps investor to see the effect that opportunity costs have on the cash 
        flow they get from an initial investment. This allows the investor or analyst to
        see the affect that time has on the actual value of the money returned enabling him
        to compound earnings to reinvest all interests, dividends, and capital gains.
    </p>
</div>
    
<div class="container-fluid" style="margin-top:50px;">
    <div class="d-flex justify-content-center">
        
        <div class="container bg-light mb-5">
            <div class="row">
            
            <div class="col-lg">
                <div class="p-2">
                    <h4>Please select the Time Value Money Calculator</h4>
                    <h4>you want to use: </h4>
                </div>

                <form action="/calculator/timeval" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md p-3">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="options" id="fval" value="1" checked>
                                <label class="form-check-label" for="fval">
                                    Calculate Future Value
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="options" id="pval" value="2">
                                <label class="form-check-label" for="pval">
                                    Calculate Present Value
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md p-3">
                            <div class="form-group">
                                <label for="amount">Amount <span class="text-danger">*</span></label>
                                <input type="text" name="amount" id="amount" class="form-control rounded-0" placeholder="Amount" required>
                            </div>
                        
                            <div class="form-group">
                                <label for="annual_interest_rate">Annual Interest Rate(%) <span class="text-danger">*</span></label>
                                <input type="text" name="annual_interest_rate" id="annual_interest_rate" class="form-control rounded-0" placeholder="Interest Rate" required>
                            </div>
                        
                            <div class="form-group">
                                <label for="number_of_years">Number of Years <span class="text-danger">*</span></label>
                                <input type="text" name="number_of_years" id="number_of_years" class="form-control rounded-0" placeholder="Number of Years" required>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success rounded-0">Submit</button>
                    <button type="reset" class="btn btn-success rounded-0">Reset</button>
                </form>
            </div>

            <div class="col-lg bg-white">
                <div class="bg-light p-2">
                    <h3>Result</h3>
                </div>
                <div class="p-2">
                    @if(Session::has('data'))
                        {{session()->flash('data')}}
                    @endif
                </div>
            </div>
        
            </div>
        </div>

    </div>
</div>

@endsection