@extends('layout')
@section('content')

<div class="container">
    <h1 class="display-4 mt-4 ml-2">Welcome to Jamapunji</h1>
</div>

<div class="container-fluid" style="margin-top:100px; text-align:center">
    <div class="row d-flex justify-content-center">
        <div class="mx-4 my-3">
          <div class="card" style="width: 18rem;">
            <a href="/calculator/1"><img src="{{asset('/img/1.jpg')}}" class="card-img-top" alt="..."></a>
            <div class="card-body">
              <p class="card-text">Investment Return Calculator</p>
            </div>
          </div>
        </div>
          
        <div class="mx-4 my-3">
          <div class="card" style="width: 18rem;">
            <a href="/calculator/2"><img src="{{asset('/img/2.jpg')}}" class="card-img-top" alt="..."></a>
            <div class="card-body">
              <p class="card-text">Compound Interest Calculator</p>
            </div>
          </div>
        </div>

        <div class="mx-4 my-3">
          <div class="card" style="width: 18rem;">
              <a href="/calculator/3"><img src="{{asset('/img/3.png')}}" class="card-img-top" alt="..."></a>
              <div class="card-body">
                <p class="card-text">Time Value of Money Calculator</p>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection