<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/calculator/{id}', "CalculatorController@cal");
Route::post('/calculator/timeval', "CalculatorController@timeval");
Route::post('/calculator/investmentreturn', "CalculatorController@investmentreturn");
Route::post('/calculator/compoundinterest', "CalculatorController@compoundinterest");